---
title: "Introduction"
date: 2019-10-19T13:26:50+02:00
anchor: "Introduction"
weight: 10
---

CargoTube allows application to communicate with each other without the need to know where
the peer application is located.


